
Sub-path URL Aliasing 6.x-1.x, xxxx-xx-xx
-----------------------------------------
by smk-ka: Changed code to use the advanced drupal_static() pattern.
by smk-ka: Fixed queried sub-paths don't respect alias order
 (based on core bug #358315 drupal_lookup_path() not respects alias' order).
#576868 by HnLn: Fixed wrong order of queried sub-paths in multilingual
 environments, causing the edit link of a node that has a parent node in the
 menu hierarchy (and a path that is built based on this hierarchy) to link back
 to the view of the parent node instead of the edit page.
by smk-ka: Removed unnecessary column aliasing from SQL queries.
by smk-ka: Replaced strncmp() with an easier to understand logic.


Sub-path URL Aliasing 6.x-1.1, 2009-11-02
-----------------------------------------
by smk-ka: Added support for path alias whitelist (see
 http://drupal.org/node/106559).
#613318 by Dave Reid: Updated hook_url_alter function names.
 Note: requires URL Alter version 1.2 or later!


Sub-path URL Aliasing 6.x-1.0, 2009-06-20
-----------------------------------------
Initial release.
